# influxdb-kafka-demo

This repo contains an example for how to use Kafka, Telegraf, and InfluxDB Cloud v3 together to monitor meteo data from OpenWeatherMap.

This repo is based on repo: https://github.com/influxcommunity/influxdb-kafka-demo

## Run
To run this example repo follow these steps: 
1. Create a file (named .env) into resources folder and put the following key/value pairs:
   2. INFLUXDB_TOKEN=your-token-from-influx 
   3. ORG=your-org-from-influx
   4. INFLUX_URL=your-url-influx-oncloud
   5. API_KEY=your-api-key-from-openwheathermap
2. Add your InfluxDB Cloud v3 url, token, org, and bucket into .env file created before
2. Run the containers with `docker-compose up --build`
3. Wait 30 seconds before Telegraf is ready to write metrics
