import time
import json
from kafka import KafkaProducer
import requests
from toml import load
import logging
import os


def get_weather(api_key, city):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=metric"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        weather_info = {
            "temperature": data["main"]["temp"],
            "description": data["weather"][0]["description"],
            "humidity": data["main"]["humidity"],
            "wind_speed": data["wind"]["speed"],
            "city": data["name"],
            "country": data["sys"]["country"]
        }
        return weather_info
    else:
        print("Failed to fetch weather data:", response.status_code)
        return None

def main():
    # Configure logging
    logging.basicConfig(filename='/app/logs/log_file.log', level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()

    # Retrive configuration file from config.toml
    config = load('/app/config/config.toml')
    api_key = os.environ['API_KEY']
    topic_name = config["general"]["topic_name"]
    data_polling_time = config["general"]["data_polling_time"]
    while True:
        city = config["general"]["city"]
        weather = get_weather(api_key, city)
        if weather:
            json_data = json.dumps(weather)
            # Transmit data to kafka
            producer = KafkaProducer(bootstrap_servers=['kafka:9092'])

            producer.send(f'{topic_name}', bytes(f'{json_data}', 'UTF-8'))
            logger.info(f"Sensor data is sent: {json_data}")

        else:
            logger.error("Failed to fetch weather data.")
        time.sleep(data_polling_time)

if __name__ == "__main__":
    main()

